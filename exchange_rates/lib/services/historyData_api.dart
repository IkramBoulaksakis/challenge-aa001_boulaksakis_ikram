import 'package:exchangerates/models/historyDataModel.dart';
import 'package:http/http.dart';
import 'package:date_format/date_format.dart';

class historyDataApi {
  Client _client = Client();

  Future<HistoryDataModel> fetchHistoryData() async {
    var today = formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd]);
    var before = formatDate(
        DateTime.now().subtract(Duration(days: 60)), [yyyy, '-', mm, '-', dd]);
    print(before);
    Response response = await _client.get(
        "https://api.exchangeratesapi.io/history?start_at=$before&end_at=$today&base=USD");
    if (response.statusCode == 200) {
      print("Success");
      return historyDataModelFromJson(response.body);
    }
    print("Fail");
    return HistoryDataModel();
  }
}
