
import 'package:exchangerates/models/latestRatesModel.dart';
import 'package:http/http.dart';

class LatestRatesApi {
  Client _client = Client();

  Future<latestRatesModel> fetchLatestRates() async {
    Response response =
        await _client.get("https://api.exchangeratesapi.io/latest?base=USD");

    if (response.statusCode == 200) {
      print("Success");
      return latestRatesModelFromJson(response.body);
    }
    print("Fail");

    return latestRatesModel();
  }
}
