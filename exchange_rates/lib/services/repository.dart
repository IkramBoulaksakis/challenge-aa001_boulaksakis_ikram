import 'package:exchangerates/providers/historyData_provider.dart';
import 'package:exchangerates/providers/latestRates_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class Repository {
  Future<void> fetchRates(BuildContext context) async {
    latestRatesProvider ratesProvider = Provider.of(context, listen: false);
    await ratesProvider.fetchRates();
  }

  Future<void> fetchHistoryData(BuildContext context) async {
    HistoryDataProvider dataProvider = Provider.of(context, listen: false);
    await dataProvider.fetchData();
  }

}
