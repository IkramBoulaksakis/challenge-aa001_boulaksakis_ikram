import 'package:exchangerates/providers/historyData_provider.dart';
import 'package:exchangerates/providers/latestRates_provider.dart';
import 'package:exchangerates/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(
  MultiProvider(providers: [
    ChangeNotifierProvider(create: (_) => latestRatesProvider()),
    ChangeNotifierProvider(create: (_) => HistoryDataProvider()),
  ],
  child: MaterialApp(
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
  ),)
);

