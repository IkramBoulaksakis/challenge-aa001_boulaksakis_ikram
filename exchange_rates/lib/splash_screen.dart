import 'package:exchangerates/services/repository.dart';
import 'package:exchangerates/views/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Repository repository = Repository();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_callApi);
  }

  _callNextPage() {
    Navigator.of(context)
        .pushReplacement(CupertinoPageRoute(builder: (_) => FirstView()));
  }

  _callApi(_) async {
    await repository.fetchRates(context);
    _callNextPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
        ),
      ),
    );
  }
}
