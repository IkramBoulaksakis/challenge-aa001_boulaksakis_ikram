import 'package:exchangerates/models/latestRatesModel.dart';
import 'package:exchangerates/providers/latestRates_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'second_view.dart';

class FirstView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Currency App",
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(40.0),
          child: Padding(
            padding: EdgeInsets.only(right: 20, left: 20, bottom: 20),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Currency : ",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                  ),
                  Row(
                    children: [
                      Text(
                        "Usd",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.arrow_forward_ios_rounded,
                        color: Colors.black,
                        size: 20,
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      body: Consumer<latestRatesProvider>(
        builder: (_, latestRatesProvider latestRates, __) {
          latestRatesModel model = latestRates.rates;
          var entryList = model.rates.entries.toList();
          return ListView.builder(
            itemBuilder: (_, index) {
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SecondView(
                      base : entryList[index].key
                    )),
                  );
                },
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 20,
                      bottom: 20,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(entryList[index].key),
                            SizedBox(
                              height: 10,
                            ),
                            Text(entryList[index].value.toString())
                          ],
                        ),
                        Icon(
                          Icons.show_chart_sharp,
                          size: 20,
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
            itemCount: entryList.length,
          );
        },
      ),
    );
  }
}
