import 'package:exchangerates/models/historyDataModel.dart';
import 'package:exchangerates/providers/historyData_provider.dart';
import 'package:exchangerates/services/repository.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:provider/provider.dart';

class SecondView extends StatefulWidget {
  final String base;

  const SecondView({Key key, this.base}) : super(key: key);
  @override
  _SecondViewState createState() => _SecondViewState();
}

class _SecondViewState extends State<SecondView> {
  Repository repository = Repository();

  _callApi(_) async {
    await repository.fetchHistoryData(context);
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(_callApi);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        body: Consumer<HistoryDataProvider>(
            builder: (_, HistoryDataProvider historyDataProvider, __) {
          HistoryDataModel model = historyDataProvider.historyDataModel;
          if (model == null) {
            return Center();
          }
          var keysList = model.rates.keys.toList();
          var valuesList = model.rates.values.toList();
          List<DateTime> l = [];
          for (int j = 0; j < keysList.length; j++) {
            l.add(DateTime.parse(keysList[j]));
          }
          l.sort((a, b) => a.compareTo(b));
          List<String> finalKeysList = [];
          for (int j = 0; j < keysList.length; j++) {
            String m = formatDate(l[j], [yyyy, '-', mm, '-', dd]);
            finalKeysList.add(m);
          }
          if (keysList == null) {
            return Center();
          }
          var list3 = [];
          for (int j = 0; j < model.rates.length; j++) {
            var list1 = valuesList[j].keys.toList();
            var list2 = valuesList[j].values.toList();
            double t;
            for (int m = 0; m < list1.length; m++) {
              if (list1[m].toString() == "${widget.base}") {
                t = list2[m];
                list3.add(t);
              }
            }
          }
          List<FlSpot> spot = [];
          for (var i = 0; i < keysList.length; i++) {
            var x = i + 1;
            spot.add(FlSpot(i.toDouble(), list3[i]));
          }
          print(spot);
          return ListView.builder(
            itemBuilder: (_, index) {
              if (keysList == null) {
                return Center();
              }
              return Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.2,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Historical rates for the past two months",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 20),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text("1 USD    >>     ${widget.base}")
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                          padding: EdgeInsets.only(right: 20, top: 10),
                          child: LineChart(
                            LineChartData(
                                titlesData: FlTitlesData(
                                  bottomTitles: SideTitles(
                                    showTitles: false,
                                  ),
                                ),
                                borderData: FlBorderData(
                                  show: true,
                                  border: const Border(
                                    bottom: BorderSide(
                                        color: Colors.grey, width: 1),
                                    left: BorderSide(
                                        color: Colors.grey, width: 0.5),
                                    right: BorderSide(
                                        color: Colors.grey, width: 0.5),
                                    top: BorderSide(
                                        color: Colors.grey, width: 0.5),
                                  ),
                                ),
                                gridData: FlGridData(
                                  show: true,
                                  getDrawingHorizontalLine: (value) {
                                    return FlLine(color: Colors.grey[200]);
                                  },
                                  getDrawingVerticalLine: (value) {
                                    return FlLine(color: Colors.grey[200]);
                                  },
                                  drawVerticalLine: true,
                                  drawHorizontalLine: true,
                                ),
                                lineBarsData: [
                                  LineChartBarData(
                                    spots: spot,
                                    barWidth: 1,
                                    dotData: FlDotData(
                                      show: false,
                                    ),
                                  )
                                ]),
                          )),
                    )
                  ],
                ),
              );
            },
            // itemCount: model.rates.length,
            itemCount: 1,
          );
        }));
  }
}
