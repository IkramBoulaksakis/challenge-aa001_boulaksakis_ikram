import 'dart:convert';

latestRatesModel latestRatesModelFromJson(String str) =>
    latestRatesModel.fromJson(json.decode(str));

String latestRatesModelToJson(latestRatesModel data) =>
    json.encode(data.toJson());

class latestRatesModel {
  latestRatesModel({
    this.rates,
    this.base,
    this.date,
  });

  Map<String, double> rates;
  String base;
  DateTime date;

  factory latestRatesModel.fromJson(Map<String, dynamic> json) =>
      latestRatesModel(
        rates: Map.from(json["rates"])
            .map((k, v) => MapEntry<String, double>(k, v.toDouble())),
        base: json["base"],
        date: DateTime.parse(json["date"]),
      );

  Map<String, dynamic> toJson() => {
        "rates": Map.from(rates).map((k, v) => MapEntry<String, dynamic>(k, v)),
        "base": base,
        "date":
            "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
      };
}
