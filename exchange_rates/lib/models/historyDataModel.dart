import 'dart:convert';

HistoryDataModel historyDataModelFromJson(String str) => HistoryDataModel.fromJson(json.decode(str));

String historyDataModelToJson(HistoryDataModel data) => json.encode(data.toJson());

class HistoryDataModel {
  HistoryDataModel({
    this.rates,
    this.startAt,
    this.base,
    this.endAt,
  });

  Map<String, Map<String, double>> rates;
  DateTime startAt;
  String base;
  DateTime endAt;

  factory HistoryDataModel.fromJson(Map<String, dynamic> json) => HistoryDataModel(
    rates: Map.from(json["rates"]).map((k, v) => MapEntry<String, Map<String, double>>(k, Map.from(v).map((k, v) => MapEntry<String, double>(k, v.toDouble())))),
    startAt: DateTime.parse(json["start_at"]),
    base: json["base"],
    endAt: DateTime.parse(json["end_at"]),
  );

  Map<String, dynamic> toJson() => {
    "rates": Map.from(rates).map((k, v) => MapEntry<String, dynamic>(k, Map.from(v).map((k, v) => MapEntry<String, dynamic>(k, v)))),
    "start_at": "${startAt.year.toString().padLeft(4, '0')}-${startAt.month.toString().padLeft(2, '0')}-${startAt.day.toString().padLeft(2, '0')}",
    "base": base,
    "end_at": "${endAt.year.toString().padLeft(4, '0')}-${endAt.month.toString().padLeft(2, '0')}-${endAt.day.toString().padLeft(2, '0')}",
  };
}