import 'package:exchangerates/models/historyDataModel.dart';
import 'package:exchangerates/models/latestRatesModel.dart';
import 'package:exchangerates/services/historyData_api.dart';
import 'package:exchangerates/services/latestRates_api.dart';
import 'package:flutter/cupertino.dart';

class HistoryDataProvider with ChangeNotifier {

  HistoryDataModel _historyDataModel = HistoryDataModel();
  historyDataApi _dataApi = historyDataApi();

  HistoryDataModel get historyDataModel => _historyDataModel;
  set historyDataModel(HistoryDataModel historyDataModel) {
    _historyDataModel = historyDataModel;
    notifyListeners();
  }

  Future<void> fetchData() async {
    historyDataModel = await _dataApi.fetchHistoryData();
  }
}
