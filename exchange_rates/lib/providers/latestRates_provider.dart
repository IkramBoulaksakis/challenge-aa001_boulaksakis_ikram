import 'package:exchangerates/models/latestRatesModel.dart';
import 'package:exchangerates/services/latestRates_api.dart';
import 'package:flutter/cupertino.dart';

class latestRatesProvider with ChangeNotifier {
  latestRatesModel _rates = latestRatesModel();
  LatestRatesApi _latestRatesApi = LatestRatesApi();

  latestRatesModel get rates => _rates;
  set rates(latestRatesModel rates) {
    _rates = rates;
    notifyListeners();
  }

  Future<void> fetchRates() async {
    rates = await _latestRatesApi.fetchLatestRates();
  }
}
